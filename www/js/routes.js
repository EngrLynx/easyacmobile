angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('tabsController.control', {
    url: '/control',
    views: {
      'tab1': {
        templateUrl: 'templates/control.html',
        controller: 'controlCtrl'
      }
    }
  })

  .state('tabsController.priceWatch', {
    url: '/watch',
    views: {
      'tab2': {
        templateUrl: 'templates/priceWatch.html',
        controller: 'priceWatchCtrl'
      }
    }
  })

  .state('tabsController', {
    url: '/page1',
    templateUrl: 'templates/tabsController.html',
    abstract:true
  })

$urlRouterProvider.otherwise('/page1/control')

  

});